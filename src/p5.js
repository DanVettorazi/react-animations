import React, {useEffect} from 'react';
import './App.css';
import p5 from 'p5';
const Canvas = () => {
//P5.js brings all the processing ideas to the web. It's very easy to start a project of P5 on a old and good vanilla-DOM...
//But we are working on React. So the installation is quite ugly to start for the first time.
//we need to reference the DOM's canvas to React:

let sketchRef = React.useRef(); //binding this variable to the dom reference

let Sketch = (p) => {
    let radius = 1;
    let increment=true;
    p.setup = () => {
      p.createCanvas(window.innerWidth, 480);
    }
    p.draw = () => {
      if (p.mouseIsPressed) {
        p.fill(`#${Math.floor(Math.random() * (999999))}`);//Using template 
      } else {
        p.fill("#FFFFFF");
      }
      p.ellipse(p.mouseX, p.mouseY, radius, radius);
      if(radius>200 && increment){
          increment=false;
      }
      else if(radius<0  && !increment){
        increment=true;
      }
      if(increment){
        radius++;
      }else{
          radius--;
      }
    }
 }
 useEffect(() => {
    let myP5 = new p5(Sketch, sketchRef.current); //creating a new P5 with the reference created.
 }, [])

    return (
        <div ref={sketchRef}></div>
    );
}

//More about using references on React's docs: https://reactjs.org/docs/refs-and-the-dom.html
export default Canvas;
