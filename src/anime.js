import React, {useState, useEffect} from 'react';
import './App.css';
import anime from "animejs";
function Anime() {

// This is an example of how to use anime.js with React and Hooks.
// Anime.js is a lightweight JavaScript animation library with a simple, yet powerful API. It works with CSS properties, SVG, DOM attributes and JavaScript Objects.
// https://github.com/juliangarnier/anime/
// Pros:
// A lot of examples on their website.
// Easy to install and to use it.
// Compatible with all browsers.
// intuitive way to create timelines and animations.
// Cons:
// It holds heavily on CSS properties to build animations and CSS inside of a React file isn't a normal CSS. so you need to have this in mind when 
// Styiling components in an responsive layout can be a problem.A

  const [play, SetPlay]= useState(false);

  let animateDiv =()=>{
    SetPlay(true)
    anime({
      targets: ".anime_div",
      translateX: "450px",
      rotate: "0.5turn",
      backgroundColor: "#32a852",
      duration: 1500,
      direction: "normal"//normal= 0-1 | alternate= 0-1-0 | reverse= 1-0
    });
  }

//the values of translation are relative to the first position of the element.
//in other words: If have a object on [100,50] (x,y) and wants to go to [300,150] you need to write like this:
//translateX: 200, translateY: 100
  let animateTimeline = () => {
    const tl = anime.timeline({
      easing: 'easeOutExpo',
      duration: 750,
      direction: "alternate"
    });

    tl
    .add({
      targets: '.anime_secdiv',
      translateX: 250,
    })
    .add({
      targets: '.anime_secdiv',
      translateY: -220,
      backgroundColor: '#32a852',
    })
    .add({
      targets: '.anime_secdiv',
      translateX: 0,//Note that the the first position X of the element is 0 and not -250.
    });
  };



  useEffect(() => {
    console.log(play)
    if(play){
      animateTimeline();
      SetPlay(false)
    }
    //useEffects runs when the DOM is first loaded (bc you setted the play to false), and when the play changes the state.
    return () => {
    //cleanupcode
    }
  }, [play])
  return (
    <div className="App">
    <div className="anime_div" onClick={animateDiv}><h1 className="titleBox">This Div has an animation triggering a Hook on it.<br/>Click me!</h1></div>
    <div className="anime_secdiv" onClick={animateTimeline}><h1 className="titleBox">This DIV is controlled<br/>by a Timeline</h1></div>

    </div>
  );
}
export default Anime;
