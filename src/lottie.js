import React, {useEffect, useRef} from 'react';
import Lottie from 'lottie-web'
import './App.css'

function Lottiecomp() {
  const container = useRef(null);
  useEffect(()=>{
    Lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      autoplay:true,
      animationData:require('./animlottie.json'),
      loop:true,
    })
  })
  return (
    <div>
      <div className="container" ref={container}></div>
    </div>
  );
}

export default Lottiecomp;
