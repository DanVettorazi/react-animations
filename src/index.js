import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import Anime from './anime'
import Gsap from './gsap'
import Canvas from './p5'
import Three from './three'
import LottieComp from './lottie'
ReactDOM.render(
  <React.StrictMode>
  <h1>Example using GSAP:</h1>
    <Gsap />
  <h1>Example using Anime.js:</h1>
    <Anime />
  <h1>Example using P5.js (without any React Wrapper):</h1>
    <Canvas/>
  <h1>Example using Three.js (Without three-fiber-react):</h1>
    <Three/>
  <h1>Using Lottie (loading a SVG):</h1>
    <LottieComp/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
