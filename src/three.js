import React, {useEffect} from 'react';
import * as THREE from "three";
const Three = () => {
    let threecanvas = React.useRef(null);

    useEffect(() => {

       // create the scene and renderer for the animation
        const scene = new THREE.Scene();
        let camera = new THREE.PerspectiveCamera(75, window.innerWidth / 400, 0.1, 1000);
        const renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth, 400);
        threecanvas.current.appendChild(renderer.domElement);

        const geometry = new THREE.BoxGeometry(1, 1, 1);
        const material = new THREE.MeshNormalMaterial();
        const cube = new THREE.Mesh(geometry, material);
        scene.add(cube);
        camera.position.z = 5;
        const animate = function () {
          requestAnimationFrame(animate);
          cube.rotation.x += 0.01;
          cube.rotation.y += 0.01;
          renderer.render(scene, camera);
        };
        animate();
    }, [])
        return (
            <div>
                 <div className="threecanvas" ref={threecanvas} />
            </div>
        )
    }

export default Three;