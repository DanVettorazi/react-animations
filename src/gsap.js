import React, {useState, useEffect} from 'react';
import './App.css';
import { gsap } from "gsap";
function Gsap() {

// This is an example of how to use GSAP (Green sock Animation Platform) with React.
//https://greensock.com/
//GSAP is a very famous and old animation module. It became famous on the Flash Era and as one of the first animation library to animate on HTML5
//It's easy to install and to animate DOM elements, as DIVS or texts.
//GSAP has a big community, that already created wrappers to use to animate Three.js scenes or, on Canvas Animations.
//It offers a wide range of Easing, options and plugins.

const animateDiv = () => {
    //gsap.to creates a one part animation. It
    gsap.to(".gsap_div", { duration: 3, x: 300, rotation: 360, scale: 0.5});
    // gsap.to(".gsap_div", { duration: 1, x: 0, rotation: 0, scale: 1,  delay: 3 }); // 👈Uncomment this to add more keyframes on your animation
  };

//👆This will play just one animation and will stop. Yes, you can add more 'keyframes' on it and will work (try to uncomment) the line above.
//But What if you wanted to make the first animation longer? 
//You'd need to adjust every delay thereafter. And what if you want to pause()
//the whole sequence or restart() it or reverse()
//it on-the-fly or repeat it twice? This could become quite messy,
//but GSAP's Timelines make it incredibly simple:

  const animateTimeline = () => {
    let tl = gsap.timeline({
        repeat:0,
        repeatDelay:2, //this doesn't make sense on this particular example.It's just to show how to add more properties on a timeline.
    })
    tl.to('.gsap_secdiv',{ duration: 1, x: 800, rotation: 360, scale: 0.5});
    tl.to('.gsap_secdiv',{ duration: 0.5, y: 300, scale: 1});
    tl.to('.gsap_secdiv',{ duration: 1, x: 200, scale: 1});
    tl.to('.gsap_secdiv',{ duration: 1, y: 0, scale: 0.5});
    tl.to('.gsap_secdiv',{ duration: 1, x: 0, scale: .7});
    tl.to('.gsap_secdiv',{ duration: 1, rotation: 360, scale: 1});
};

  return (
    <div className="App">
    <div className="gsap_div" onClick={animateDiv}><h1 className="titleBox">This Div has an animation buildin.<br/>Click me!</h1></div>
    <div className="gsap_secdiv" onClick={animateTimeline}><h1 className="titleBox">This DIV is controlled<br/>by a Timeline</h1></div>
    </div>
  );
}
export default Gsap;
