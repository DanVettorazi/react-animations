# react-animations
<div align="center"><img src="images/top.gif" /></div>
<div align="center">A couple of examples to show how to integrate popular animation modules with React.js.</div>


# Libraries presented in this project:
*  Three.js (vanilla Three.js, without the use of Three-react-fiber)
*  Greensock.js
*  P5.js (P5.js without any React wrapper)
*  Anime.js
*  Lottie.js
*  two.js




# Objective of this research:
<div align="center"><img src="images/sample.gif" /></div>
A new workflow brings new questions and challenges to any team. So the reason for this document is to show how to implement old libraries, or how to use more common workflows, without
the need of using additional modules or wrappers.

# Why not just use some React wrapper?
Understanding how to implement old libraries or how to manipulate the DOM is a necessary knowledge. Not only to not depend on third parties to update their code, but also to create lighter React applications.
Taking React-three-fiber as an example: It's a great way to have faster re-usable components using three.js, but this and others reconciliers can turn a simple workflow in a big headache if you
have a three.js scene using some third-party plugin, or if you are trying to bring legacy code to a new React application. In this cases, to use a wrapper can hinder your process.

## Greensock animation platform
A.k.a GSAP, is a very famous and old animation module. It became famous on the Flash Era and was one of the first animation library to work on HTML5
- It's easy to install and to animate DOM elements, as DIVS or texts.
- GSAP has a big community, that already created wrappers to use to animate Three.js scenes or, on Canvas Animations.
- It offers a wide range of Easing, options and plugins.

## P5.js (P5.js without any React wrapper)
P5.js brings all the processing concept to the web. It's very easy to start a project of P5.js on a old and good vanilla-DOM(?) But on React, the process can take additional steps.
The setup is quite ugly to start for the first time. In the example, heavily commented, I referenced the DOM's canvas to React.

## Anime.js
Anime.js provides a basic API that lets you animate almost anything you can think of. With Anime.js, you can do basic animations where you move objects back and forth, or you can do more advanced animations where you restyle a component with an action.
Anime.js also offers support for things like timelines, where you can create an animated sequence of events. This is particularly useful when it comes to presenting several events at once.

## Three.js (vanilla Three.js, without the use of Three-react-fiber)
Three.js is a cross-browser JavaScript library and application programming interface used to create and display animated 3D computer graphics in a web browser. Three.js uses WebGL.

## Lottie.js
Lottie is a lightweight SVG animation loader. It allows you to have control of any loaded svg.

# List of Animation Modules to be added:
* two.js
* gl-react
* ogl
* mo.js
* -popmotion- (Looking at popmotion I found out his younger brother: https://www.framer.com/motion/ It looks very attractive.)


# Author

Daniel Vettorazi
